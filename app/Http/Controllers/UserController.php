<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuthException;
use JWTAuth;
use App\Models\{User};

class UserController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        $token = null;
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'response' => 'error',
                    'message' => 'invalid_email_or_password',
                ], 400);
            }
        } catch (JWTAuthException $e) {
            return response()->json([
                'response' => 'error',
                'message' => 'failed_to_create_token',
            ], 500);
        }

        $user = JWTAuth::user();

        return response()->json(compact('token', 'user'));
    }

    public function register(Request $request)
    {
        try {
            $data = $request->all();
            $data['name'] = $request->email;

            $user = User::create($data);
            $password = bcrypt($request->password);
            $user->password = $password;
            $user->save();
        } catch (\Exception $e) {
            return response()->json([
                'response' => 'error',
                'message' => 'this_email_is_already_in_the_system'
            ], 400);
        }

        return response()->json([
            'user' => $user
        ], 200);
    }
}
