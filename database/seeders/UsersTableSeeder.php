<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'admin', 'email' => 'admin@gmail.com', 'password' => bcrypt('password')]
        ];

        foreach ($items as $item) {
            User::create($item);
        }
    }
}
