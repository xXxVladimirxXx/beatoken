import VueRouter from "vue-router"

const main = () => import('./components/Main')

const home = () => import('./components/Home')
const drops = () => import('./components/Drops')
const dropJoin = () => import('./components/DropJoin')
const dropBuy = () => import('./components/DropBuy')
const dropCount = () => import('./components/DropCount')
const dropList = () => import('./components/DropList')
const dropRedirect = () => import('./components/DropRedirect')
const dropOpen = () => import('./components/DropOpen')
const logged = () => import('./components/Logged')
const signup = () => import('./components/Signup')
const login = () => import('./components/Login')

export default {
    routes: new VueRouter({
        mode: 'history',
        routes: [
            {
                path: '',
                name: 'main',
                component: main,
                children: [
                    {
                        path: '/',
                        name: 'home',
                        component: home
                    },
                    {
                        path: '/drops',
                        name: 'drops',
                        component: drops
                    },
                    {
                        path: '/drop-join',
                        name: 'dropJoin',
                        component: dropJoin
                    },
                    {
                        path: '/drop-buy',
                        name: 'dropBuy',
                        component: dropBuy
                    },
                    {
                        path: '/drop-open',
                        name: 'dropOpen',
                        component: dropOpen
                    },
                    {
                        path: '/logged',
                        name: 'logged',
                        component: logged
                    },
                ]
            },
            {
                path: '/drop-count',
                name: 'dropCount',
                component: dropCount
            },
            {
                path: '/drop-list',
                name: 'dropList',
                component: dropList
            },
            {
                path: '/drop-redirect',
                name: 'dropRedirect',
                component: dropRedirect
            },
            {
                path: '/signup',
                name: 'signup',
                component: signup
            },
            {
                path: '/login',
                name: 'login',
                component: login
            }
        ]
    })
}
