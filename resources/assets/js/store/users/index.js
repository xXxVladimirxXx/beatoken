import axios from 'axios'

const bufToken = localStorage.getItem('user-token')
const bufUser = JSON.parse(localStorage.getItem('user'))

if (bufToken) {
    axios.defaults.headers.common.Authorization = `Bearer ${bufToken}`
}

export default {
    namespaced: true,
    state: {
        token: bufToken || "",
        user: bufUser || {},
    },
    actions: {
        login({ commit }, user) {
            return new Promise((resolve, reject) => {
                axios({ url: "/api/login", data: user, method: "POST" })
                    .then(resp => {
                        const token = resp.data.token
                        const user = resp.data.user

                        localStorage.setItem("user-token", token)
                        localStorage.setItem("user", JSON.stringify(user))
                        axios.defaults.headers.common.Authorization = `Bearer ${token}`
                        commit("user-token", token)
                        commit("setUser", user)

                        resolve(resp)
                    })
                    .catch(err => {
                        localStorage.removeItem("user-token")
                        localStorage.removeItem("user")
                        reject(err)
                    });
            });
        },
        register({ commit }, user) {
            return new Promise((resolve, reject) => {
                axios({ url: "/api/register", data: user, method: "POST" })
                    .then(resp => {
                        const token = resp.data.token
                        const user = resp.data.user

                        resolve(resp)
                    })
                    .catch(err => {
                        localStorage.removeItem("user-token")
                        localStorage.removeItem("user")
                        reject(err)
                    });
            });
        },
        logout({ state }) {
            state.token = ""
            state.user = null;
            localStorage.removeItem("user-token")
            localStorage.removeItem("user");
        }
    },
    mutations: {
        setUser(state, user) {
            state.user = user
        },
    },
    getters: {
        getUser(state) {
            return state.user
        }
    }
};
