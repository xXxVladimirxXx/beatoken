require('./bootstrap');

import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import modules from './store';
import beatokenRoutes from './beatoken-routes';

import App from './App'

Vue.use(Vuex)
Vue.use(VueRouter)

export const store = new Vuex.Store({
    ...modules
})

new Vue({
    el: '#beatoken-root',
    router: beatokenRoutes.routes,
    store,
    render: h => h(App)
})
